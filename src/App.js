import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import UsersBox from './components/UsersBox/UsersBox';

class App extends Component {
    render() {
        return (
            <div className="App">
                <UsersBox />
            </div>
        );
    }
}

export default App;
