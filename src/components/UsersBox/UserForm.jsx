import React, { PropTypes, Component } from 'react';
import './user-form.css';

const propTypes = {
    addUser: PropTypes.func,
    updateUser: PropTypes.func
};

class UserForm extends Component {
    constructor() {
        super();

        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleChange = this.handleChange.bind(this);
    }

    componentWillMount() {
        this.setUser(this.props.updatedUser);
    }

    componentWillReceiveProps(nextProps) {
        this.setUser(nextProps.updatedUser);
    }

    setUser(user) {
        this.setState({
            id: user === null ? null : user.id,
            username: user === null ? '' : user.username,
            email: user === null ? '' : user.email,
            phone: user === null ? '' : user.phone
        });
    }

    handleSubmit(event) {
        event.preventDefault();

        const { id, username, email, phone } = this.state;

        if (id === null) {
            this.props.addUser({
                username,
                email,
                phone
            });
        } else {
            this.props.updateUser({
                id,
                username,
                email,
                phone
            })
        }

        this.setState({
            id: null,
            username: '',
            email: '',
            phone: ''
        });
    }

    handleChange(event) {
        this.setState({
            [event.target.name]: event.target.value
        });
    }

    render() {
        return (
            <div className="formBox">
                <form onSubmit={this.handleSubmit}>
                    <div className="formBlock">
                        <div className="leftFormBlock">
                            <div>
                                <label className="formLabel" htmlFor="username">Имя</label>
                            </div>
                            <div>
                                <input name="username" type="text" value={this.state.username} onChange={this.handleChange} />
                            </div>
                        </div>
                        <div className="rightFormBlock">
                            <div>
                                <label className="formLabel" htmlFor="phone">Телефон</label>
                            </div>
                            <div>
                                <input name="phone" type="text" value={this.state.phone} onChange={this.handleChange} />
                            </div>
                        </div>
                    </div>
                    <div className="formBlock">
                        <div className="leftFormBlock">
                            <div>
                                <label className="formLabel" htmlFor="email">E-Mail</label>
                            </div>
                            <div>
                                <input name="email" type="email" value={this.state.email} onChange={this.handleChange} />
                            </div>
                        </div>
                        <div className="rightFormBlock">
                            <input className="saveButton" type="submit" value="Сохранить" />
                        </div>
                    </div>
                    <i className="fa fa-angle-up leftAngle" aria-hidden="true"></i>
                    <i className="fa fa-angle-up rightAngle" aria-hidden="true"></i>
                </form>
            </div>
        );
    }
}

UserForm.propTypes = propTypes;

export default UserForm;