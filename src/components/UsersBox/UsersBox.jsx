import React, { PropTypes, Component } from 'react';
import User from './User';
import UserForm from './UserForm';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as usersActions from '../../redux/actions/usersActions';

import './users-box.css';

const propTypes = {
    users: PropTypes.array,
    fetchUsersRequest: PropTypes.func,
    createUserRequest: PropTypes.func,
    updateUserRequest: PropTypes.func,
    deleteUserRequest: PropTypes.func
};

class UsersBox extends Component {
    constructor() {
        super();

        this.state = {
            showForm: false,
            updatedUser: null
        };

        this.addUser = this.addUser.bind(this);
        this.deleteUser = this.deleteUser.bind(this);
        this.updateUser = this.updateUser.bind(this);
        this.showFormCreation = this.showFormCreation.bind(this);
        this.showFormChanges = this.showFormChanges.bind(this);
    }

    componentDidMount() {
        this.props.fetchUsersRequest();
    }

    addUser(user) {
        this.props.createUserRequest(user).then(() => {
            this.setState({
                showForm: false
            });
        });
    }

    deleteUser(id) {
        this.props.deleteUserRequest(id).then(() => {
            this.setState({
                showForm: false
            });
        });
    }

    updateUser(user) {
        this.props.updateUserRequest(user).then(() => {
            this.setState({
                updatedUser: null,
                showForm: false
            });
        });
    }

    showFormCreation() {
        this.setState({
            showForm: true,
            updatedUser: null
        });
    }

    showFormChanges(user) {
        this.setState({
            showForm: true,
            updatedUser: user
        });
    }

    render() {
        const { showForm, updatedUser } = this.state;
        const usersJsx = this.props.users.map((user, index) => (
            <User key={index} id={index} user={user} deleteUser={this.deleteUser} showFormChanges={this.showFormChanges} />
        ));
        return (
            <div className="users-box">
                <h3>Клиенты</h3>
                <span id="addUserButton" onClick={this.showFormCreation}>
                    <i id="addPlus" className="fa fa-plus-circle" aria-hidden="true"></i>
                    &nbsp;
                    Добавить клиента
                </span>
                { showForm && <UserForm updatedUser={updatedUser} addUser={this.addUser} updateUser={this.updateUser} /> }
                { usersJsx.length > 0 && <table className="usersTable">
                    <thead>
                        <tr>
                            <th></th>
                            <th>Клиент</th>
                            <th>Телефон</th>
                            <th>E-Mail</th>
                            <th>Дата последнего посещения</th>
                            <th>Сумма оплат</th>
                            <th>Количество посещений</th>
                            <th>Активный абонемент</th>
                        </tr>
                    </thead>
                    <tbody>
                        { usersJsx }
                    </tbody>
                </table> }
            </div>
        );
    }
}

UsersBox.propTypes = propTypes;

function mapStateToProps(state) {
    const users = state.users;
    return { users };
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators(usersActions, dispatch);
}

const Box = connect(mapStateToProps, mapDispatchToProps)(UsersBox);

export default Box;