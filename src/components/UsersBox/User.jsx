import React, { PropTypes, Component } from 'react';
import './user.css';

const propTypes = {
    deleteUser: PropTypes.func,
    updateUser: PropTypes.func
};

class User extends Component {
    constructor() {
        super();

        this.handleDelete = this.handleDelete.bind(this);
        this.handleUpdate = this.handleUpdate.bind(this);
    }

    handleDelete() {
        this.props.deleteUser(this.props.id);
    }

    handleUpdate() {
        const { id } = this.props;
        const { username, email, phone } = this.props.user;
        this.props.showFormChanges({
            id,
            username,
            email,
            phone
        });
    }

    render() {
        const {username, email, phone} = this.props.user;
        return (
            <tr>
                <td>
                    <i onClick={this.handleUpdate} className="fa fa-pencil tableFont" aria-hidden="true"></i>
                    &nbsp;
                    <i onClick={this.handleDelete} className="fa fa-times tableFont" aria-hidden="true"></i>
                </td>
                <td>{ username }</td>
                <td>{ email }</td>
                <td>{ phone }</td>
                <td> ------ </td>
                <td> ------ </td>
                <td> ------ </td>
                <td> ------ </td>
            </tr>
        );
    }
}

User.propTypes = propTypes;

export default User;