import {
    FETCH_USERS_STARTED,
    FETCH_USERS_FINISHED,
    FETCH_USERS_ERROR,
    
    CREATE_USER_STARTED,
    CREATE_USER_FINISHED,
    CREATE_USER_ERROR,
    
    UPDATE_USER_STARTED,
    UPDATE_USER_FINISHED,
    UPDATE_USER_ERROR,
    
    DELETE_USER_STARTED,
    DELETE_USER_FINISHED,
    DELETE_USER_ERROR
} from '../actions/usersActions';

const initialState = {
    users: [],
    error: null,
    isFetching: false
};

export default function (state = initialState, action) {
    switch (action.type) {
        case FETCH_USERS_STARTED:
        case FETCH_USERS_ERROR:
            return Object.assign({}, state, {
                error: action.error,
                isFetching: action.isFetching
            });
        case FETCH_USERS_FINISHED:
            return Object.assign({}, state, {
                users: action.users,
                error: action.error,
                isFetching: action.isFetching
            });
        case CREATE_USER_STARTED:
        case CREATE_USER_ERROR:
            return Object.assign({}, state, {
                isFetching: action.isFetching,
                error: action.error
            });
        case CREATE_USER_FINISHED:
            return Object.assign({}, state, {
                users: [...state.users, action.user],
                error: action.error,
                isFetching: action.isFetching
            });
        case UPDATE_USER_STARTED:
        case UPDATE_USER_ERROR:
            return Object.assign({}, state, {
                error: action.error,
                isFetching: action.isFetching
            });
        case UPDATE_USER_FINISHED:
            return Object.assign({}, state, {
                users: state.users.map((user, index) => {
                    if (index === action.user.id) {
                        return action.user;
                    }
                    return user;
                }),
                error: action.error,
                isFetching: action.isFetching
            });
        case DELETE_USER_STARTED:
        case DELETE_USER_ERROR:
            return Object.assign({}, state, {
                error: action.error,
                isFetching: action.isFetching
            });
        case DELETE_USER_FINISHED:
            return Object.assign({}, state, {
                users: state.users.filter((user, index) => index !== action.id),
                error: action.error,
                isFetching: action.isFetching
            });
        default:
            return state;
    }
}