export const FETCH_USERS_STARTED = 'FETCH_USERS_STARTED';
export const FETCH_USERS_FINISHED = 'FETCH_USERS_FINISHED';
export const FETCH_USERS_ERROR = 'FETCH_USERS_ERROR';

export const CREATE_USER_STARTED = 'CREATE_USER_STARTED';
export const CREATE_USER_FINISHED = 'CREATE_USER_FINISHED';
export const CREATE_USER_ERROR = 'CREATE_USER_ERROR';

export const UPDATE_USER_STARTED = 'UPDATE_USER_STARTED';
export const UPDATE_USER_FINISHED = 'UPDATE_USER_FINISHED';
export const UPDATE_USER_ERROR = 'UPDATE_USER_ERROR';

export const DELETE_USER_STARTED = 'DELETE_USER_STARTED';
export const DELETE_USER_FINISHED = 'DELETE_USER_FINISHED';
export const DELETE_USER_ERROR = 'DELETE_USER_ERROR';

function fetchUsersStarted() {
    return {
        type: FETCH_USERS_STARTED,
        isFetching: true,
        error: null
    };
}
  
function fetchUsersError(error) {
    return {
        type: FETCH_USERS_ERROR,
        isFetching: false,
        error
    };
}
  
function fetchUsersFinished(users) {
    return {
        type: FETCH_USERS_FINISHED,
        users,
        isFetching: false,
        error: null
    };
}
  
function createUserStarted() {
    return {
        type: CREATE_USER_STARTED,
        isFetching: true,
        error: null
    };
}

function createUserError(error) {
    return {
        type: CREATE_USER_ERROR,
        isFetching: false,
        error
    };
}

function createUserFinished(user) {
    return {
        type: CREATE_USER_FINISHED,
        user,
        isFetching: false,
        error: null
    };
}
  
function updateUserStarted() {
    return {
        type: UPDATE_USER_STARTED,
        isFetching: true,
        error: null
    };
}

function updateUserError(error) {
    return {
        type: UPDATE_USER_ERROR,
        isFetching: false,
        error
    };
}

function updateUserFinished(user) {
    return {
        type: UPDATE_USER_FINISHED,
        user,
        isFetching: false,
        error: null
    };
}
  
function deleteUserStarted() {
    return {
        type: DELETE_USER_STARTED,
        isFetching: true,
        error: null
    };
}

function deleteUserError(error) {
    return {
        type: DELETE_USER_ERROR,
        isFetching: false,
        error
    };
}

function deleteUserFinished(id) {
    return {
        type: DELETE_USER_FINISHED,
        id,
        isFetching: false,
        error: null
    };
}
  
export function fetchUsersRequest() {
    return (dispatch) => {
        dispatch(fetchUsersStarted());
        let promise = new Promise((resolve, reject) => {
            resolve([]);
        });
        return promise.then((response) => {
            dispatch(fetchUsersFinished(response));
        }).catch((error) => {
            dispatch(fetchUsersError(error));
        });
    };
}
  
export function createUserRequest(user) {
    return (dispatch) => {
        dispatch(createUserStarted());
        let promise = new Promise((resolve, reject) => {
            resolve(user);
        });
        return promise.then((response) => {
            dispatch(createUserFinished(response));
        }).catch((error) => {
            dispatch(createUserError(error));
        });
    };
}
  
export function updateUserRequest(user) {
    return (dispatch) => {
        dispatch(updateUserStarted());
        let promise = new Promise((resolve, reject) => {
            resolve(user);
        });
        return promise.then((response) => {
            dispatch(updateUserFinished(response));
        }).catch((error) => {
            dispatch(updateUserError(error));
        });
    };
}
  
export function deleteUserRequest(id) {
    return (dispatch) => {
        dispatch(deleteUserStarted());
        let promise = new Promise((resolve, reject) => {
            resolve(id);
        });
        return promise.then((response) => {
            dispatch(deleteUserFinished(response));
        }).catch((error) => {
            dispatch(deleteUserError(error));
        });
    };
}